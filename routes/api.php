<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
/**rotas get */
Route::get('vehicles/{n1}','Vehicle@index');
Route::get('brands/{n1}','Vehicle_brand@index');
Route::get('query/{n1}','Querie@index');
Route::get('execute/query/{n1}','Querie@exeQuery');
Route::get('options','Vehicles_optional@index');
Route::get('models/{n1}','Vehicles_model@index');
Route::get('fuels','Vehicles_fuel@index');
Route::get('colors','Vehicles_color@index');
Route::get('segments','Vehicles_segment@index');
Route::get('exchanges','Vehicles_exchange@index');
Route::get('versions/{n1}/{n2}','Vehicles_version@index');
Route::get('vehicles_delete_img/{n1}','Vehicle@destroy');
Route::get('dealers/{n1}','Vehicle_dealers@index');
Route::get('show_vehicles_publisheds/{n1}','Vehicle@show_vehicles_publisheds');

/**rotas post */
Route::post('show_vehicles_publisheds/{n1}','Vehicle@show_vehicles_publisheds');
Route::post('insert/models','Vehicles_model@insert');
Route::post('vehicles_save_img/{n1}','Vehicle@save_img');
Route::post('vehicles_pull/{n1}','Vehicle@edit');
Route::post('insertoptions/vehicles','Vehicle@insertOptions');
Route::post('insert/versions','Vehicles_version@insert');
Route::post('vehicles/{n1}','Vehicle@index');
Route::post('insert/vehicles','Vehicle@insert');
Route::post('execute/query/{n1}','Querie@exeQuery');

/**Rotas Delete */
Route::delete('delete/query/{n1}','Querie@delete');


// Route::resource('vehicles', 'Vehicle');
Route::resource('companies', 'Compans')->middleware(Auth::class);
