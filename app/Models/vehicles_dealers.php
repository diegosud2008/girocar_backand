<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class vehicles_dealers extends Model
{
    //
    public function list_dealers($company) {
       return $this::select(['name','lf_company_id'])->where('lf_company_id',$company)->get();
    }
}
