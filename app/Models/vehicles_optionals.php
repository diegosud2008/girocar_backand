<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class vehicles_optionals extends Model
{
    //
    public function options_vehicles($vehicle_id = "") {
        return $this::
        select(['vehicles_optionals.name'])
        ->leftjoin('vehicles_optionals_rel as op','op.vehicle_optional_id','=','vehicles_optionals.id')
        ->where('op.vehicle_id',$vehicle_id)
        ->get();
    }
}
