<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class vehicles_published_portals extends Model
{
    public function portals_vehicles($vehicle_id = "") {
        return $this::
        select(['vd.name','vehicles_published_portals.status','vehicles_published_portals.return'])
        ->leftjoin('vehicles_dealers as vd','vd.id','=','vehicles_published_portals.vehicle_dealer_id')
        ->where('vehicles_published_portals.vehicle_id', $vehicle_id)
        // ->where('vehicles_published_portals.status','published')
        ->get();
    }
    public function portals_vehicles_status($company = "", $request) {
        return $this::
        select(['vd.id'])
        ->leftjoin('vehicles as vd','vd.id','=','vehicles_published_portals.vehicle_id')
        ->where('vd.lf_company_id', $company)
        ->where('vehicles_published_portals.status',$request)
        ->groupBy('vd.id')
        ->get();
    }
}
