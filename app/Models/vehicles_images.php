<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class vehicles_images extends Model
{
    //
    protected $fillable = [
        'id',
        'vehicle_id',
        'type',
        'image',
        'order',
        'webmotors_code',
        'icarros_code',
        'created_at',
        'updated_at'
    ];
    public function images($vehicle_id = "") {
        return $this::
        select(['image as url','id as uid','type as name',DB::raw('CONCAT("IMAGEM") as status')])
        ->where('vehicle_id',$vehicle_id)
        // ->whereNotNull('image')
        ->orderBy('order','asc')
        ->get();
    }
}
