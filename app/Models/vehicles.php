<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vehicles extends Model
{
    //
    public $result = "";
    private function get_list() {
        return $this::
        select(['vehicles.*','vb.vehicle_category_id','vm.vehicle_brand_id','vv.vehicle_model_id','vi.image','vv.name as version','vm.name as model', 'vb.name as brand' , 'vf.name as fuel'])
        ->leftjoin('vehicles_versions as vv', 'vv.id','=','vehicles.vehicle_version_id')
        ->leftjoin('vehicles_models as vm', 'vm.id', '=', 'vv.vehicle_model_id')
        ->leftjoin('vehicles_brands as vb', 'vb.id','=', 'vm.vehicle_brand_id')
        ->leftjoin('vehicles_categories as vc', 'vc.id', '=', 'vb.vehicle_category_id')
        ->leftjoin('vehicles_fuels as vf', 'vf.id', '=', 'vehicles.vehicle_fuel_id')
        ->leftjoin('vehicles_images as vi', function($join) {
            $join->on('vi.vehicle_id', '=', 'vehicles.id')
            ->where('vi.type','ARQUIVO1');
        });
    }
    public function all_vehicles($company = "")
    {
        $this->result = $this->get_list()->where('vehicles.lf_company_id',$company)->orderBy('vehicles.id', 'desc')->paginate(10);
        return $this->result;
    }
    public function filter_vehicles($company = "", $request = "") {
        $this->result = $this->get_list()->where('vehicles.lf_company_id',$company);


        if ($request['UPDATED']) {

            $this->result = $this->result->where('vehicles.created_at','<=',DB::raw('NOW()'))
            ->where('vehicles.created_at','>',DB::raw('DATE_SUB(NOW(), INTERVAL 30 DAY)'));

        }
        return $this->result->paginate(10);
    }

    public function pull($vehicle_id, $request = []) {
        try {
            $this::where('id',$vehicle_id)->update($request);
            return true;
        } catch (\Throwable $th) {
            return $th;
        }
    }
}
