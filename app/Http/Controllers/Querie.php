<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\queries;
use Illuminate\Support\Facades\DB;
class Querie extends Controller
{
    //
    public function index($id_company = ""){

        return queries::where('lf_company_id',$id_company)->get();
    }

    public function exeQuery($id_company = "",request $request) {
        if ($request['add'] === 'add') {
            $insert = array(
                'lf_company_id' => $id_company,
                'title' => $request['title'] === 'undefined' ? NULL : $request['title'],
                'type' => $request['type'] === 'undefined' ? NULL : $request['type'],
                'sql' => $request['sql'] === 'undefined' ? NULL : $request['sql'],
                'host' => $request['host'] === 'undefined' ? NULL : $request['host'],
                'database' => $request['database'] === 'undefined' ? NULL : $request['database'],
                'username' => $request['username'] === 'undefined' ? NULL : $request['username'],
                'password' => $request['password'] === 'undefined' ? NULL : $request['password']);
            queries::insert($insert);
        }
        if ( $request['type'] === 'SQL Server' ) {
            $pdo = new \PDO('sqlsrv:Server=' . $request['host'] .';Database='. $request['database'] .';', $request['username'] , $request['password']);

            $quer = $pdo->prepare($request['sql']);
            $quer->execute();
            return $quer->fetchAll(\PDO::FETCH_OBJ);
        }
        if ( $request['type'] === 'DealerUp/NBS' ) {
            $xml = simplexml_load_file($request['host']);
            return json_encode($xml);
        }
    }

    public function delete($id = "") {
        return queries::where('id',$id)->delete();
    }
}
