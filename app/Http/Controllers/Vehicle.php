<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\vehicles;
use App\Models\vehicles_published_portals;
use App\Models\vehicles_optionals;
use App\Models\vehicles_images;
use App\Models\vehicles_optionals_rel;
use Aws\S3\S3Client;


class Vehicle extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $vehicles;
    protected $vehicles_published_portals;
    protected $vehicles_options;
    protected $vehicles_options_rel;
    protected $vehicles_images;
    public function __construct(vehicles $vehicles,vehicles_published_portals $vehicles_published_portals, vehicles_optionals $vehicles_options,vehicles_images $vehicles_images,vehicles_optionals_rel $vehicles_optionals_rel)
    {

        // $this->middleware('auth');
        $this->vehicles = $vehicles;
        $this->vehicles_published_portals = $vehicles_published_portals;
        $this->vehicles_options = $vehicles_options;
        $this->vehicles_options_rel = $vehicles_optionals_rel;
        $this->vehicles_images = $vehicles_images;

    }

    public function index($company, Request $request)
    {
        if ($request['WHERE']) {
            $this->vehicles =  $this->vehicles->filter_vehicles($company,$request);
        } else {
            $this->vehicles = $this->vehicles->all_vehicles($company);
        }
        $vehicles = [];
        foreach( $this->vehicles as $key => $vehicle) {
            $vehicle['published_portals'] = $this->vehicles_published_portals->portals_vehicles($vehicle['id']);
            $vehicle['options'] = $this->vehicles_options->options_vehicles($vehicle['id']);
            $vehicle['images'] = $this->vehicles_images->images($vehicle['id']);
        }
        return $this->vehicles;

    }
    public function show_vehicles_publisheds($company) {
        $vehicles_publisheds = array("portals_" . $_GET['published'] => count($this->vehicles_published_portals->portals_vehicles_status($company,$_GET['published'])));
        return json_encode($vehicles_publisheds, true);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //
        return $this->vehicles->pull($id,$request->all());
    }

    public function save_img(Request $request, $id)
    {
        //
        DB::connection('mysql2')->table('nt_girocar_imagens')->where('CHAVE',$request['type'])->where('PLACA',$request['board'])->update(['IMAGEM' => $request['image'],'CHAVE' => $request['type']]);
        if ($request['typedb'] === 'insert') {
            $data = vehicles_images::insert(['image' => $request['image'],'type' => $request['type'],'order' => $request['order'],'type' => $request['type'],'vehicle_id' => $id]);
        } else {
            $data = vehicles_images::where('type',$request['type'])->where('vehicle_id',$id)->update(['image' => $request['image'],'type' => $request['type'],'order' => $request['order']]);
        }
        return json_encode(array("image" => $request['image'],"id" => $id));
    }
    public function destroy($id)
    {
        $delete = vehicles_images::where('id',$id)->delete();
        return $delete;
    }

    public function insert(request $request) {
        $vehicle = $this->vehicles->where('board',$request['board'])->get();

        if (count($vehicle)){
            return $vehicle[0]['id'];
        } else {
            // return $this->vehicles->insert($request->all())->id;
        }
    }

    public function insertOptions(request $request) {
        $options = DB::table('vehicles_optionals_rel')->where($request->all())->get();
        if (count($options)){
            return $options;
        } else  {
            return DB::table('vehicles_optionals_rel')->insert($request->all());
        }
    }
}
