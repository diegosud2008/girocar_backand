<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\vehicles_models;

class Vehicles_model extends Controller
{
    //
    public function index($vehicle_brand_id) {
        return vehicles_models::where('vehicle_brand_id',$vehicle_brand_id)->get();
    }

    public function insert(request $requet) {
        vehicles_models::insert($requet->all());
    }
}
