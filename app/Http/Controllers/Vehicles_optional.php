<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\vehicles_optionals;

class Vehicles_optional extends Controller
{
    //
    public function index() {
        return vehicles_optionals::orderBy('name')->get();
    }

}
