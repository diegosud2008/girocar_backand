<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\vehicles_colors;

class Vehicles_color extends Controller
{
    //
    public function index()
    {
        return vehicles_colors::all();
    }
}
