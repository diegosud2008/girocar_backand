<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\vehicles_segments;

class Vehicles_segment extends Controller
{
    //
    public function index() {
        return vehicles_segments::all();
    }
}
