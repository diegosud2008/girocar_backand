<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\vehicles_brands;
class Vehicle_brand extends Controller
{
    //
    public function index($vehicle_category_id) {
        return vehicles_brands::where('vehicle_category_id',$vehicle_category_id)->get();
    }
}
