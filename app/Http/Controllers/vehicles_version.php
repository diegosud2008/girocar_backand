<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\vehicles_versions;

class vehicles_version extends Controller
{
    //
    public function index($vehicles_versios_id = 0,$year_model) {
        return vehicles_versions::where('vehicle_model_id',$vehicles_versios_id)->where('final_year',$year_model)->get();
    }
    public function insert(request $request) {
        vehicles_versions::insert($request->all());
        return true;
    }
}
