<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\vehicles_dealers;
class Vehicle_dealers extends Controller
{
    protected $vehicles_dealers;
    public function __construct(vehicles_dealers $vehicles_dealers)
    {
        // $this->middleware('auth');
        $this->vehicles_dealers = $vehicles_dealers;
    }
    public function index($company) {
        return $this->vehicles_dealers->list_dealers($company);
    }
}
