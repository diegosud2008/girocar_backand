
import React, { useEffect, useState } from 'react';
import {
    Link,
    Route,
    Redirect
  } from "react-router-dom";

import { check_use } from './Components/functions/check_user';
import { seach_companies,set_companie } from './Components/functions/search_companies';
import 'antd/dist/antd.css';
import './global.css';
import { Layout, Menu, Breadcrumb,Select ,Row,Col,Avatar, Badge,Input } from 'antd';
import {
  DesktopOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
  CarOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  HomeOutlined
} from '@ant-design/icons';
import { mdiBellRing  } from '@mdi/js';
import Icon from '@mdi/react';
import Logo from './Components/imgs/logo_girocar.png';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
const { Search } = Input;
const { Option } = Select;


export const PrivateRoute = ({ component: Component, ...rest }) => {
    let [collapsed,setCollapsed] = useState(true);
    let [companies,setCompanie] = useState([]);
    let [companie, setCompani] = useState()
    useEffect(() => {
      setCompanie(seach_companies())
      setCompani(set_companie())
    },[])
    function onChangeCompany(value) {
      setCompani(value);
      localStorage.setItem('setCompany',value)
      rest.onChange(value);
    }
    function onCollapse() {
      setCollapsed(!collapsed)
    }
    function detectar_mobile(props) {
      if( navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i)
      ){
         return (
          <Layout>
            {/* <Sider collapsible collapsed={collapsed} onCollapse={(collapsed) => onCollapse(collapsed)}> */}

              <Header className="site-layout-background" style={{ position: 'fixed', zIndex: 2, width: '100%' }}>
                <Menu theme="light" defaultSelectedKeys={['1']} mode="horizontal" style={{ zIndex: 9, width: '100%' }}>
                  <Menu.Item key="1" icon={<CarOutlined />}/>
                  <Menu.Item key="2" icon={<DesktopOutlined />}/>
                  <SubMenu key="sub1" icon={<UserOutlined />}>
                    <Menu.Item key="3">Tom</Menu.Item>
                    <Menu.Item key="4">Bill</Menu.Item>
                    <Menu.Item key="5">Alex</Menu.Item>
                  </SubMenu>
                  <SubMenu key="sub2" icon={<TeamOutlined />}>
                    <Menu.Item key="6">Team 1</Menu.Item>
                    <Menu.Item key="8">Team 2</Menu.Item>
                  </SubMenu>
                  <Menu.Item key="9" icon={<FileOutlined />}/>
                </Menu>
                </Header>
              {/* </Sider> */}
              <Layout className="site-layout">
                <Header className="site-layout-background" style={{ padding: 0 }} />
                <Content style={{ margin: '0 16px' }}>
                  <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>User</Breadcrumb.Item>
                    <Breadcrumb.Item>Bill</Breadcrumb.Item>
                  </Breadcrumb>
                  <div style={{  minHeight: '75vh' }}>
                    <Component {...props} />
                  </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>© 2021 Lead Force. Todos os direitos reservados.</Footer>
              </Layout>
            </Layout>
          );
       }
      else {
        let style = {
          'padding': '24px',
          // 'height': '82vh',
          // 'display': 'block',
          // 'position': 'fixed',
          // 'width': '90%',
          // 'overflowX': 'hidden',
          // 'overflowY': 'AUTO'
        }
         return <Layout>
           <Header className="site-layout-background header-custom p-0">
                <Row className="custom-header">

                  <Col span={12} xs={24} md={19} className="align-items-center d-flex">
                    <div className="logo" >
                      <img src={Logo} alt=""/>
                    </div>
                    {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                      className: 'trigger mr-1 font-28',
                      onClick: () => onCollapse(),
                    })}
                    <Search placeholder="Busca Rápida" className="mr-2 input-seach" onSearch={console.log("teste")} />
                    <Select
                        showSearch
                        style={{ width: 400 }}
                        placeholder="Empresas"
                        optionFilterProp="children"
                        value={companie}
                        onChange={(value) => onChangeCompany(value)}
                    >
                        {
                        companies.map( (companie,index) => {
                            return <Option value={companie.token} key={index} >{companie.nome}</Option>
                        })
                        }
                    </Select>
                  </Col>
                  <Col span={12} xs={24} md={5} className="d-flex justify-content-right align-items-center">
                    <Badge count={99} className="mr-4">
                        <Icon path={mdiBellRing} color="rgba(0, 0, 0, 0.45)" size={1}/>
                    </Badge>
                    <Avatar size={44} icon={<UserOutlined />} />
                </Col>
                </Row>
              </Header>
              <Layout>
                <Sider collapsible collapsed={collapsed} trigger={null} theme="light" width={290}>
                    <Menu theme="light" className="menu" defaultSelectedKeys={['1']} mode="inline">
                      <Menu.Item key="1" icon={<HomeOutlined style={{ fontSize: 20}}/>}>
                        <Link to='/dashboard'>Dashboard</Link>
                      </Menu.Item>
                      <SubMenu key="sub1" icon={<CarOutlined style={{ fontSize: 20}}/>} title="Meu estoque">
                        <Menu.Item key="3">Adicionar Veículo</Menu.Item>
                        <Menu.Item key="4">Gerenciar Estoque</Menu.Item>
                        <Menu.Item key="5">Vendidos</Menu.Item>
                        <Menu.Item key="6">Excluidos</Menu.Item>
                      </SubMenu>
                      <SubMenu key="sub2" icon={<TeamOutlined />}>
                        <Menu.Item key="6">Team 1</Menu.Item>
                        <Menu.Item key="8">Team 2</Menu.Item>
                      </SubMenu>
                      <Menu.Item key="9" icon={<FileOutlined />}>
                        <Link to="/Query">Consulta</Link>
                      </Menu.Item>
                    </Menu>
                  </Sider>
                  <Layout className="site-layout">
                    <Content style={{ margin: '0 16px' }}>
                      <div className="mt-2" style={style}>
                        <Component {...props} companie={companie} />
                      </div>
                    </Content>
                  <Footer style={{ textAlign: 'center' }}>© 2021 Lead Force. Todos os direitos reservados.</Footer>
                </Layout>
           </Layout>
         </Layout>;
       }
     }
    return (
    <Route
      {...rest}
      render={props =>
        check_use() ? (
          detectar_mobile(props)
        ) : (
          <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
        )
      }
    />
  )
}
