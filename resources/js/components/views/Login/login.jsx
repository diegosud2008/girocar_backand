
import React, { Component } from 'react';
import { Row, Col,Form, Input, Button, Checkbox, notification } from 'antd';
import { save_user } from '../../functions/check_user';
import  Logins  from '../../../styles/Login';
import {
    Redirect
  } from "react-router-dom";

export default class Login extends Component {
    state = {
        loadings: false,
        log: this.props.check_use
    }
    onFinish = async (values) => {
        this.setState({
            loadings: true
        })

        let log = await save_user(values);
        console.log(log)
        if (!log) {
            notification.error({
                message: 'Error Login',
                description:
                  'Usuário ou senha incorreto'
              })
        }

        this.setState({
            log,
            loadings: false
        })
    };

    render() {

        let { log,loadings } = this.state;
        let { isMobile } = this.props;
        if( log ) {
            return <Redirect to="/vehicles"/>
        }

        const layout = {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 },
        };
        const tailLayout = {
        wrapperCol: { span: 24 },
        };
        return(
            <Logins>
                <Row className="gutter-row">
                    { !isMobile &&
                        <Col sm={15} id="left">
                            <img src="https://crm7.com.br/wp-content/uploads/2020/03/6-maneiras-pr%C3%A1ticas-de-melhorar-as-vendas-automatizando-seus-fluxos-de-trabalho-de-CRM.jpg" alt=""/>
                        </Col>
                    }
                    <Col sm={9} xs={24}>
                        <div className="top">
                            <img src="https://app.girocar.com.br/assets/img/logo.png?v=1617332083" alt="" className="img"/>
                        </div>
                        <main className="body">
                            <h1>Seja bem vindo</h1>
                            <h3>Para continuar, digite seu usuário e senha</h3>
                            <Form
                                {...layout}
                                name="login"
                                initialValues={{ remember: true }}
                                onFinish={this.onFinish}
                                >
                                <Form.Item
                                    name="username"
                                    rules={[{ required: true, message: 'Por favor insira seu usuário' }]}
                                >
                                    <Input placeholder="Usuário"/>
                                </Form.Item>

                                <Form.Item
                                    name="password"
                                    rules={[{ required: true, message: 'Por favor insira sua senha' }]}
                                >
                                    <Input.Password placeholder="Senha" />
                                </Form.Item>

                                <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                                    <Checkbox>Permanecer Logado</Checkbox>
                                </Form.Item>

                                <Form.Item {...tailLayout}>
                                    <Button type="primary" htmlType="submit" loading={loadings}>
                                        Entrar
                                    </Button>
                                </Form.Item>
                            </Form>
                            <Col span={19} id="end-body">
                                <hr/>precisa de ajuda ?<hr/>
                                <div className="links">
                                    <a href="">(51) 3061-6037</a>
                                    <a href="">atendimento@leadforce.com.br</a>
                                </div>
                            </Col>
                        </main>
                        <footer>
                            <span>© 2021 Lead Force. Todos os direitos reservados.</span>
                            <img src="https://app.girocar.com.br/assets/img/logo-lead.png?v=1617332083" alt=""/>
                        </footer>
                    </Col>
                </Row>
            </Logins>
        )
    }
}
