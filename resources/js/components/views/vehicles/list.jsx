
import React from 'react';
import { List, Avatar, Space } from 'antd';
import { MessageOutlined, LikeOutlined, StarOutlined } from '@ant-design/icons';
import Icon from '@mdi/react'
import { mdiCarWindshieldOutline } from '@mdi/js';
const listData = [];
for (let i = 0; i < 23; i++) {
  listData.push({
    href: 'https://ant.design',
    title: `Renault Logan`,
    avatar: 'https://http2.mlstatic.com/D_NQ_NP_607478-MLB27450551369_052018-O.jpg',
    description:
      'A vida em modo conforto.',
    // content:
    //   'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  });
}

const IconText = ({ icon, text }) => (
  <Space style={{display: 'flex',alignItems: 'center'}}>
    <Icon path={icon} color="rgba(0, 0, 0, 0.45)" size={1.3} style={{paddingTop: '10px'}}/>
    <b>{text}</b>
  </Space>
);

export default function list_vehicles(){
    return(
        <List
            itemLayout="vertical"
            size="large"
            pagination={{
            onChange: page => {
                console.log(page);
            },
            pageSize: 7,
            }}
            dataSource={listData}
            footer={
            <div>
                <b>ant design</b> footer part
            </div>
            }
            renderItem={item => (
            <List.Item
                key={item.title}
                actions={[
                <IconText icon={mdiCarWindshieldOutline} text="Placa: KOY2F20" key="list-vertical-star-o" />,
                <IconText icon={LikeOutlined} text="Marca: Renault" key="list-vertical-like-o" />,
                <IconText icon={MessageOutlined} text="Modelo: Voyage" key="list-vertical-message" />,
                <IconText icon={MessageOutlined} text="Versão: Voyage 1.6 Total Flex" key="list-vertical-message" />,
                ]}
                extra={
                <img
                    width={272}
                    alt="logo"
                    src="https://www.renault.com.br/agg/vn/unique/ONE_CF_DACIA_NEQ_DENSITY1_LARGE/r_brandSite_carPicker_1.png?uri=https%3A%2F%2Fbr.co.rplug.renault.com%2Fproduct%2Fmodel%2FL4M%2Flogan%2Fc%2FA-ENS_0MDL2P1SERIELIM1_-TEKNG"
                />
                }
            >
                <List.Item.Meta
                avatar={<Avatar src={item.avatar} />}
                title={<a href={item.href}>{item.title}</a>}
                description={item.description}
                />
                {item.content}
            </List.Item>
            )}
        />
    ) 
}