
import React, { Component } from 'react';
import List_vehicles from './list';

export default class Vehicles extends Component {
    render() {
        return(
            <div className="content" style={{width: '100%'}}>
                <List_vehicles/>
            </div>
        )
    }
}