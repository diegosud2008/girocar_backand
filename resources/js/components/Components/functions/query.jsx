
import { vehicles_brands,vehicles_models,vehicle_versions,Vehicles_fuels,Vehicles_exchanges,Vehicles_colors,insertVersion,insertModel,insertVehicles,Options } from './vehicles';

export async function Query(id_company) {
    let Querys = fetch(`http://127.0.0.1:8000/api/query/${id_company}`).then( dados => dados.json());

    return Querys;
}

export async function DadoQuery(id,id_company) {
    let Querys = await fetch(`http://127.0.0.1:8000/api/query/${id_company}`).then( dados => dados.json());
    let query = Querys.length > 0 ? Querys.filter( q => q.id === parseInt(id))[0] : {};
    return query;
}

export async function executeQuery(id_company, params = {}) {
    var formdata = new FormData();    
    
    Object.keys(params).map( (value) => {
        formdata.append(String(value), String(params[value]));
        return true;
    });

    let execute = await fetch(`http://127.0.0.1:8000/api/execute/query/${id_company}`,{
        method: 'POST',
        body: formdata
    }).then(dados => dados.json());

    return execute;
}

function buscaColum(chave,value) {
    let Colum = Object.keys(chave).filter( colum => colum.toLocaleLowerCase().includes(value))
    return Colum[0]
}

function buscaDado(_array,campo,valor) {
    return _array.filter( arr => arr[campo].toLocaleLowerCase().includes(valor.toLocaleLowerCase()));
}

export async function InsertVehicle(vehicles = [], id_company = '') {
    let brands = await vehicles_brands(1);
    let optionsVeh = await Options();
    let i = 0;
    vehicles.map( async veh => {
        let vehicle = {
            vehicle_version_id: 0,
            name: "",
            lf_company_id: id_company,
            vehicle_exchange_id: 2,
            vehicle_fuel_id: 2,
            vehicle_segment_id: 2,
            vehicle_color_id: 2,
            year_manufacture: "",
            year_model: "",
            doors: null,
            board: "",
            type: "N",
            price: 0,
            price_purchase: 0,
            price_minimum: 0,
            km: 0,
            description: "",
            status: 1,
            financed: 0,
            BPS: 0,
            armored: 0,
            factory_warranty: 0,
            adapted_people_with_disabilities: 0,
            only_owner: 0,
            alienated: 0,
            ipva_paid: 0,
            revised_car_agenda_workshop: 0,
            car_dealer_reviews: 0,
            swap_accepted: 0,
            collector_vehicle: 0,
            approved_plus: 0,
            chassi: "",
            video_youtube: ""
        }
    
        let options = [];
        if ( buscaColum(veh,'fabricacao') ) {
            vehicle.year_manufacture = veh[buscaColum(veh,'fabricacao')]
        }

        if ( buscaColum(veh,'categoria') ) {

        } else {
            /** brandVehicle busca o modelo do veiculo que esta vindo da importação independete do campo desde que no nome do campo venha a palavra marca */
            let brandVehicle = brands.filter( brand => brand.name.toLocaleLowerCase().includes(veh[buscaColum(veh,'marca')].toLocaleLowerCase()));
            let modelVehicle = []
            if ( brandVehicle.length > 0 ) {
                /** modalvehicle busca o modelo do veiculo que esta vindo da importação independete do campo desde que no nome do campo venha a palavra modelo */
                modelVehicle = await vehicles_models(brandVehicle[0].id);
                modelVehicle = modelVehicle.filter( model => model.name.toLocaleLowerCase().includes(veh[buscaColum(veh,'modelo')].toLocaleLowerCase()))
                
            }
            if ( modelVehicle.length > 0 ) {
                let vehicleversion = await vehicle_versions(modelVehicle[0].id,veh[buscaColum(veh,'anomodelo')]);
                
                if (vehicleversion.length === 0) {
                    insertVersion(modelVehicle[0].id,veh[buscaColum(veh,'anomodelo')],veh[buscaColum(veh,'versao')])
                } else {
                    vehicle.vehicle_version_id = vehicleversion.filter( version => version.name.toLocaleLowerCase().includes(veh[buscaColum(veh,'versao')].toLocaleLowerCase()))[0].id
                }
            } else {

                if ( brandVehicle.length > 0 ) {
                    insertModel(veh[buscaColum(veh,'modelo')],brandVehicle[0].id)
                }
            }
        }
        if ( buscaColum(veh,'anomodelo') ) {
            vehicle.year_model = veh[buscaColum(veh,'anomodelo')];
        }
        if ( buscaColum(veh,'placa') ) {
            vehicle.board = veh[buscaColum(veh,'placa')].replace(/-/g,'');
        }
        if ( buscaColum(veh,'combustivel')) {
            let fuels = await Vehicles_fuels();
            vehicle.vehicle_fuel_id = fuels.filter( fuel => fuel.name.toLocaleLowerCase().includes(veh[buscaColum(veh,'combustivel')].toLocaleLowerCase()))[0].id;
        }
        if ( buscaColum(veh,'cambio') ) {
            let exchange = await Vehicles_exchanges();
            vehicle.vehicle_exchange_id = buscaDado(exchange,'name',veh[buscaColum(veh,'cambio')])[0].id;
        }
        if ( buscaColum(veh,'porta') ) {
            vehicle.doors = veh[buscaColum(veh,'porta')];
        }
        if ( buscaColum(veh,'km') ) {
            vehicle.km = veh[buscaColum(veh,'km')];
        }
        if ( buscaColum(veh,'cor') ) {
            let colors = await Vehicles_colors();
            vehicle.vehicle_color_id = buscaDado(colors,'name',veh[buscaColum(veh,'cor')]).length > 0 ? buscaDado(colors,'name',veh[buscaColum(veh,'cor')])[0].id : 1;
        }
        if (buscaColum(veh,'preco')) {
            vehicle.price = veh[buscaColum(veh,'preco')];
        }
        if (buscaColum(veh,'opcionais')) {
            
            options = Object.keys(veh[buscaColum(veh,'opcionais')]).length > 0 ? veh[buscaColum(veh,'opcionais')].split(',') : [];
        }
        if (buscaColum(veh,'status')) {
            vehicle.status = veh[buscaColum(veh,'status')];
        }
        if (buscaColum(veh,'approvedplus')) {
            vehicle.approved_plus = veh[buscaColum(veh,'approvedplus')];
        }
        if (buscaColum(veh,'blindado')) {
            vehicle.armored = veh[buscaColum(veh,'blindado')];
        }
        if (buscaColum(veh,'observacao')) {
            vehicle.description = veh[buscaColum(veh,'observacao')]
        }
        if (buscaColum(veh,'tipo')) {
            vehicle.type = "N";
            if (veh[buscaColum(veh,'tipo')].toLocaleLowerCase().includes('seminovo')) {
                vehicle.type = "U";
            }
        }
        if (buscaColum(veh,'nome') && Object.keys(veh[buscaColum(veh,'nome')]).length > 0) {
            
            vehicle.name = veh[buscaColum(veh,'nome')];
        } else {
            vehicle.name = String(veh[buscaColum(veh,'versao')]);
        }
        if (vehicle.vehicle_version_id) {
            let id = await insertVehicles(vehicle);

                options.map( async opt => {
                    if (buscaDado(optionsVeh,'name',opt).length > 0) {
                        let formData = new FormData();
                        formData.append('vehicle_id',id);
                        formData.append('vehicle_optional_id',buscaDado(optionsVeh,'name',opt)[0].id)
                        fetch(`http://127.0.0.1:8000/api/insertoptions/vehicles`,{
                            method: 'POST',
                            body: formData
                        })
                    }
                })
            i++;    
        }
        
    })
}

export async function DeleteQuery(id = '') {
    return fetch(`http://127.0.0.1:8000/api/delete/query/${id}`,{
        method: 'delete'
    })
}