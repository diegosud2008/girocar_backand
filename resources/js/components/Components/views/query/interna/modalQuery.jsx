
import React from 'react';
import { Modal,Table } from 'antd';

export const ModalQuery = ({dadosRequest,isModalVisible,handleOk,handleCancel,title}) => {
      const Colums = [];
      let i = 0;
      dadosRequest.veiculo.map( (value) => {
            if ( i === 0 ) {
                Object.keys(value).map( values => {
                    Colums.push({title: values,dataIndex: values})
                    return true;
                })
                i++;
            }
            return true;
        })
        
    return(
        <Modal title={`Veículos ${title}`} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={1000}>
            <div>

                <Table
                    style={{overflow: 'auto',height: '70vh'}}
                    columns={Colums}
                    dataSource={dadosRequest.veiculo}
                />
            </div>
        </Modal>
    )
}