
import React, { Component } from 'react';
import { Table, Space,Button,Popconfirm, message } from 'antd';
import { Link } from 'react-router-dom';
import { PlusOutlined } from '@ant-design/icons';
import { DeleteQuery,Query } from '../../functions/query';



export default class Listagem extends Component{
  state = {
    data: [...this.props.querys],
    columns: [
      {
        title: 'Titulo',
        dataIndex: 'title',
        key: 'title'
      },
      {
        title: 'Tipo',
        dataIndex: 'type',
        key: 'type',
      },
      {
        title: 'Ação',
        key: 'action',
        render: (text, record) => {
          return(
            <Space size="middle">
              <Button type="primary">
                <Link to={`/QueryInterna/${record.id}`}>Editar</Link>
              </Button>
              <Popconfirm
                title="Tem certeza que deseja excluir esta tarefa?"
                onConfirm={() => this.confirm(record.id)}
                onCancel={this.cancel}
                okText="Sim"
                cancelText="Não"
              >
                <Button type="primary" danger>
                  Excluir
                </Button>
              </Popconfirm>
            </Space>
          )
        },
      },
    ]
  }
  componentDidUpdate(prevProps) {
    if (prevProps.querys !== this.props.querys) {
      this.setState({
        data: [...this.props.querys],
      })
    }
  }
  confirm = async (e) => {
    await DeleteQuery(e)
    message.success('Registro excluido');
    this.setState({
      data: [...await Query(this.props.companie)]
    })
  }
  
  cancel(e) {
    message.error('Operação cancelada');
  }
  render(){
    let { data,columns } = this.state;
    console.log(data)
    return( 
      <div>
        <div className="top-left mb-1">
          <Link to={`/QueryInterna/add`} className="c-write">
            <Button type="primary" icon={<PlusOutlined />}>
              Adicionar
            </Button>
          </Link>
        </div>
        <Table columns={columns} dataSource={data} />
      </div>
    )
  }
}