
import React, { Component } from 'react';
import ListVehicles from './list';
import { Spin } from 'antd';
import { vehicles } from '../../functions/vehicles';

export default class Vehicles extends Component {
    state = {
        loading: false,
        vehicles: []
    }
    async componentDidMount () {
        this.setState({
            loading: true
        })
        let listvehicles = await vehicles(this.props.companie);
        this.setState({
            vehicles: listvehicles,
            loading: false
        })
    }
    async componentDidUpdate(prevProps) {
        if (prevProps.companie !== this.props.companie) {
            this.setState({
                loading: true
            })
            let listvehicles = await vehicles(this.props.companie)
            this.setState({
                vehicles: listvehicles,
                loading: false
            })
        }
    }
    filterPage = async (page) => {
        this.setState({
            loading: true
        })
        let listvehicles = await vehicles(this.props.companie,page)
        this.setState({
            vehicles: listvehicles,
            loading: false
        })
    }
    render() {
        return(
            <>
            {
                this.state.loading 
                ?
                    <div className="example">
                        <Spin />
                    </div>
                :
                <div className="content" style={{width: '100%'}}>
                    <ListVehicles listVehicles={this.state.vehicles} filterPage={this.filterPage} />
                </div>
            }
            </>
        )
    }
}