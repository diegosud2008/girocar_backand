
import React, { useState, forwardRef } from 'react';
import { Upload } from 'antd';
import {
  closestCenter,
  DndContext, 
  DragOverlay,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from '@dnd-kit/core';
import {
  arrayMove,
  SortableContext,
  sortableKeyboardCoordinates,
  rectSortingStrategy,
  useSortable,
} from '@dnd-kit/sortable';
import {CSS} from '@dnd-kit/utilities';
import styled from 'styled-components';
import { EyeOutlined,RestOutlined } from '@ant-design/icons';
import { save_img } from '../../functions/vehicles';


export function Fotos(props) {
  
  const [activeId, setActiveId] = useState(null);

  const [items, setItems] = useState(props.fotos)
  
  const sortedItemIds = items.map((item) => item.uid);
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );
    
  return (
    <DndContext 
      sensors={sensors}
      collisionDetection={closestCenter}
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
    >
      <SortableContext 
        items={sortedItemIds}
        strategy={rectSortingStrategy}
      >
        {items.map((foto, i) => <SortableItem onChange={props.onChange} data={props.data} foto={foto} disabled={true} index={i} key={foto.uid} handlePreview={props.handlePreview} handleDelete={props.handleDelete} id={foto.uid} />)}
      </SortableContext>
      <DragOverlay>
        {activeId ? <div><Item id={activeId} isActive={true} handlePreview={props.handlePreview} foto={items.find(foto => foto.uid === activeId)} /></div> : null}
      </DragOverlay>
    </DndContext>
  );
  
  function handleDragStart(event) {
      const {active} = event;
      setActiveId(active.id);
  }
  
  function handleDragEnd(event) {
      const {active, over} = event;
      
      if (active.id !== over.id) {
          const oldIndex = items.map(foto => foto.uid).indexOf(active.id);
          const newIndex = items.map(foto => foto.uid).indexOf(over.id);
          const newItems = arrayMove(items, oldIndex, newIndex)
          newItems.map( (itens,index) => {
            if (!itens.name.includes('ARQUIVO98') && !itens.name.includes('ARQUIVO99')){
              itens = {...itens,url: itens.url,name: `ARQUIVO${++index}`,ordem: index}
            }
            save_img(props.vehicle.id,itens.url,itens.name,itens.ordem,props.vehicle.board)
            return true;
          })
          // 
          props.atualizaVehicle({
            ...props.vehicle,
            image: newItems[0].url,
            images: [...newItems]
          })
          setItems(newItems);      
        }
      
      // setActiveId(null);
  }
}
const FotoIcon = props => {
  let use_url = props.foto.url;
  return <FotoIconStyle style={{backgroundImage: `url(${use_url && !use_url.includes('null') ?  use_url : props.index || props.index === 0  ? require(`../../imgs/icon-car-${props.index+1}.png`) : null})`}} />
}

const FotoIconStyle = styled.div`
  cursor: grab;
  height: 153px; 
  width: 153px;
  position: relative;
  border-radius: 8px;
  margin: 10px;
  background-position: center;
  background-size: cover;
  box-shadow: 0 10px 15px #5F63F220;
  & > * {
      transition: 0.1s ease;
      opacity: 0;
      pointer-events: none;
  }
  &:hover > * {
      opacity: 1;
      pointer-events: auto;
  }
`;

function SortableItem(props) {
  const {
    attributes,
    listeners,
    isDragging,
    setNodeRef,
    transform,
    transition,
  } = useSortable({id: props.id});
  const style = {
      transform: CSS.Transform.toString(transform),
      transition
  };
  
  
       if (props.foto.url && props.foto.url !== 'null') {
        return (<Item ref={setNodeRef} handlePreview={props.handlePreview} faded={isDragging} handleDelete={props.handleDelete} index={props.index} style={style} {...attributes} {...listeners} foto={props.foto}/>)
       } else {
        return (
          <Upload.Dragger
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              onChange={props.onChange}
              data={() => props.data(props.index)}
            >
                <Item handlePreview={props.handlePreview} handleDelete={props.handleDelete} index={props.index} foto={props.foto}/>
            </Upload.Dragger>
        )
       }
      
      
  
}

const Item = forwardRef(({id, faded, style, isActive = false, ...props}, ref) => {
  return (
      <FotoItemContain>
          <div {...props} ref={ref} style={{...style, opacity: faded ? 0.2 : 1}}>
              {
                  props.foto && <FotoIcon foto={props.foto} index={props.index} />
              }
          </div>
          {
          props.foto.url && props.foto.url !== 'null'
          ?
          <div className="mask">
            <EyeOutlined className="c-pointer mr-1" onClick={() => props.handlePreview(props.foto)} />
            <RestOutlined className="c-pointer" onClick={() => props.handleDelete(props.index,props)} />
          </div>
          :
          null
          }
      </FotoItemContain>
  )
});

const FotoItemContain = styled.div`
  position: relative;
  .mask {
    position: absolute;
    top: 0;
    width: 153px;
    height: 39px;
    margin: 10px;
    background: #00000075;
    color: #fff;
    border-radius: 8px;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-around;
    transition: 0.5s;
    font-size: 18px;
    cursor: pointer;
    opacity: 0;
  }
  &:hover > .mask {
    opacity: 1;
  }
  .top-opt {
      opacity: 0;
  }

  &:hover > .top-opt {
      transition: 0.5s;
      opacity: 1;
  }
`