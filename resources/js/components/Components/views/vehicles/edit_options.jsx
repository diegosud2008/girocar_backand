
import React, { useEffect, useState} from 'react';
import { Checkbox, Col, Row,Input  } from 'antd';
import { Options } from '../../functions/vehicles';

export const EditOptions = ({vehicle}) => {
    let [ options, setOptions ] = useState([]);
    let [optionsVeh, setOpionsVeh] = useState(vehicle.options)
    let [ optionsAll, setOptionsAll] = useState([]);
    useEffect( () => {
        async function option(){
            setOptions(await Options());
            setOptionsAll(await Options());
        }
        option();
    },[]);
    const onChange = (value) => {
        let optios = optionsAll.filter(opt => opt.name.toLocaleLowerCase().includes(value.target.value.toLocaleLowerCase()));
        setOptions(optios);
    }
    const onClick = (value) => {
        console.log(value)
    }
    console.log(vehicle)
    return(
        <Row gutter={15}>
           <Col className="d-flex flex-wrap p-2" span={24}>
               <div className="w-100">
                    <Input onChange={onChange} placeholder="Buscar"/>
               </div>
                {
                    options.length > 0 && options.map( opt =>{
                        return (
                            <div className="w-30 p-1">
                                <Checkbox className="text-uppercase" onClick={onClick} checked={optionsVeh.filter( opti => opti.name === opt.name).length > 0 ? true : false} >{opt.name}</Checkbox>
                            </div>
                        )
                    })
                }
           </Col> 
        </Row>
    )
}