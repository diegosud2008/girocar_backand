
import React, { Component } from 'react';
import { Upload, Modal, Row,Col,Spin } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import {Fotos} from './dropImg';
import { Vehicle_save_img,Vehicle_delete_img,buscaImgApp,save_img } from '../../functions/vehicles';
import { Pannellum } from "pannellum-react";
import Img from '../../imgs/img.jpg'

// import S3 from 'aws-s3-reactjs';

// const config = {
//   bucketName: 'leadforce-girocar',
//   dirName: 'ima', /* optional */
//   region: 'sa-east-1',
//   accessKeyId: 'AKIAIGBGGTKRR7O6BPOA',
//   secretAccessKey: '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',
//   s3Url: 'http://leadforce-girocar.s3-sa-east-1.amazonaws.com/'
// }


// const ReactS3Client = new S3(config);

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

export default class EditImages extends Component{
    state = {
        previewVisible: false,
        previewImage: '',
        previewTitle: '',
        loading: false,
        INDEX: '',
        fileList: [...this.props.vehicle.images],
      };
    
      handleCancel = () => this.setState({ previewVisible: false });
      
      async componentDidMount(){
        let { fileList } = this.state;
        this.setState({
          loading: true
        })
        let fileListApp = await buscaImgApp(this.props.vehicle.lf_company_id,this.props.vehicle.board,this.props.vehicle.id)
        
        fileListApp.map( async (file,index) => {
          await save_img(file.vehicle_id,file.image,file.type,index || 1,this.props.vehicle.board, fileList.length === 0 ? 'insert' : null)
          fileList.map( (files,index) => {
            if(file.type === files.name) {
              fileList[index] = {...fileList[index],name: file.type,url: file.image}
            }
            return true;
          })
          return true;
        })
        
        if ( fileList.length === 0 ) {
          fileList = fileListApp.map( file => {
            return { name: file.type, url: file.image,uid: file.id || Math.random(),status: 'IMAGEM'}
          })
        }
        this.props.atualizaVehicle({
          ...this.props.vehicle,
          image: fileList[0].url,
          images: [...fileList]
        })
        this.setState({
          loading: false,
          fileList
        })
      }
      
      handlePreview = async file => {
        if (!file.url && !file.preview) {
          file.preview = await getBase64(file.originFileObj);
        }
    
        this.setState({
          previewImage: file.url || file.preview,
          previewVisible: true,
          previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
        });
      };
      handleChange = async ( fileList ) => {
        let { INDEX } = this.state;
        
        if (fileList.file.status === 'uploading' && !fileList.event) {
          let index = INDEX ? INDEX : this.state.fileList.findIndex(file => (file.url === 'null' || !file.url));
          
          let arquivo =  `ARQUIVO${fileList.fileList.length+index}`
          
          
          let img = await Vehicle_save_img(fileList.file.originFileObj,this.props.vehicle.lf_company_id,this.props.vehicle.board,this.props.vehicle.id,arquivo,index,this.props.vehicle.board);
          this.setState({
            loading: true,
          })
          this.state.fileList[(fileList.fileList.length - 1)+index] = {
            "url": img.image,
            "uid": img.id+Math.random(),
            "name": arquivo,
            "status": "IMAGEM"
          }
          console.log(this.state.fileList,this.state.fileList[0].url)
          this.props.atualizaVehicle({
            ...this.props.vehicle,
            images: [...this.state.fileList],
            image: this.state.fileList[0].url
          })
          fileList.file.status = 'success';
          this.setState({
            loading: false,
            INDEX: ''
          })
        }
        // ReactS3Client
        // .uploadFile(fileList.file.originFileObj, fileList.file.originFileObj.name)
        // .then(data => console.log(data))
        // .catch(err => console.error(err))
        // console.log(await Vehicle_save_img(fileList.file.originFileObj,this.props.vehicle.lf_company_id,this.props.vehicle.board))
        // this.setState({ fileList })
      };
      handleDelete = async (i,PROPS) => {
        this.setState({
          loading: true
        })
        Vehicle_delete_img(this.props.vehicle.lf_company_id,this.props.vehicle.board,PROPS.foto.name)
        await save_img(this.props.vehicle.id,'null',PROPS.foto.name,++i,this.props.vehicle.board)
        // Vehicle_delete(PROPS.foto.uid);
        this.state.fileList[--i] = {
          "url": null,
          "uid": PROPS.foto.uid,
          "name": PROPS.foto.name,
          "status": "IMAGEM"
        }
        this.setState({
          loading: false,
        })
      }
      data = (id) => {
        console.log(id)
        this.setState({
          INDEX: id
        })
      };
      render() {
        const { previewVisible, previewImage, fileList, previewTitle,loading } = this.state;
        return (
          <>
          {
            loading
            ?
            <Spin />
            :
            <>
            <Row gutter={10}>
              <Col span={24} className="d-flex flex-wrap">
                <Fotos fotos={fileList} atualizaVehicle={this.props.atualizaVehicle} handlePreview={this.handlePreview} handleDelete={this.handleDelete} vehicle={this.props.vehicle} data={this.data} onChange={this.handleChange}/>
              </Col>
            </Row>
            <Upload.Dragger
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              onPreview={this.handlePreview}
              onChange={this.handleChange}
              maxCount={19}
              multiple={true}
              className="w-100"
            >
                <InboxOutlined />
                <p className="ant-upload-text">Clique aqui para subir as imagens</p>
            </Upload.Dragger>
            <Modal
              visible={previewVisible}
              title={previewTitle}
              footer={null}
              onCancel={this.handleCancel}
            >
              <img alt="example" style={{ width: '100%' }} src={previewImage} />
              {/* <Pannellum
                width="100%"
                height="500px"
                image={Img}
                pitch={10}
                yaw={180}
                hfov={110}
                autoLoad
                showZoomCtrl={false}
                onLoad={() => {
                  console.log("panorama loaded");
                }}
              >
                <Pannellum.Hotspot
                  type="custom"
                  pitch={31}
                  yaw={150}
                  handleClick={(evt, name) => console.log(name)}
                  name="hs1"
                />
              </Pannellum> */}
            </Modal>
            </>
            }
          </>
        );
      }
}