
import React, { useState } from 'react';
import { Modal,Steps, Button } from 'antd';
import { SolutionOutlined, SmileOutlined } from '@ant-design/icons';
import { EditInfos } from './edit_info';
import { EditOptions } from './edit_options';
import Editimages from './edit_images';

const { Step } = Steps;

export const Edit = ({edit,handleOk,handleCancel,vehicle}) => {
    const [ step, setStep ] = useState(0);
    const [ vehicleN, setVehicle ] = useState(vehicle);
    function next() {
        setStep(step+1)
    }
    function prev() {
        setStep(step-1)
    }
    function atualizaVehicle(vehicleA){
        setVehicle(vehicleA);
    }
    
    return(
        <Modal title="Edição do veículo" visible={edit} onCancel={() => handleCancel(vehicleN)} width={750}
        footer={[
            <Button key="back" onClick={() => handleCancel(vehicleN)}>
              Fechar
            </Button>,
            <Button key="submit" type="primary" className="d-none" onClick={handleOk}>
              Submit
            </Button>,
          ]}
        >
            <Steps>
                <Step status={step > 0 ? 'finish' : 'process'} className="c-pointer" key={0} onClick={() => setStep(0)} title="Informações" icon={<SolutionOutlined />}/>
                <Step status={ step === 1 ? 'process' : step > 1 ? 'finish' : 'wait' } onClick={() => setStep(1)} className="c-pointer" key={1} title="Opções" icon={<SolutionOutlined />} />
                <Step status={step === 2 ? 'process' : step > 2 ? 'finish' : 'wait'} key={2} title="Imagens" icon={<SmileOutlined />} onClick={() => setStep(2)} className="c-pointer" />
            </Steps>
            <div className="steps-content">
                {
                    step === 0 &&
                    <EditInfos vehicle={vehicle} onClick={next} atualizaVehicle={atualizaVehicle} key={vehicle.id}/>
                }
            </div>
            <div className="steps-action">
                {step < 2 && step !== 0 && (
                    <>
                        <EditOptions vehicle={vehicle} atualizaVehicle={atualizaVehicle} />
                        <Button type="primary" onClick={next}>
                            Proximo
                        </Button>
                    </>
                )}
                {step === 2 && (
                    <Editimages vehicle={vehicle} atualizaVehicle={atualizaVehicle}/>
                )}
                {step > 0 && (
                    <Button style={{ marginTop: '11px' }} onClick={prev}>
                        Voltar
                    </Button>
                )}
            </div>
        </Modal>
    )
}